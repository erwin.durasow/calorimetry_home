"""
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuch

Author : Erwin Durasow
Erstellt : 7.11.2023
Zuletzt geändert : 13.11.2023

Beschreibung:
Diese Datei enthält ein Python-Skript um Temperaturen mit Hilfe von Sensoren des Typs DS18B20 zu messen und im data Ordner abzuspeichern. Dabei werden Metadaten in einer json-file erstellt. 
"""
from functions import m_json
from functions import m_pck

# creating paths to all needed folders and files for the heat capacity experiment
data_folder_path = "data/data_heat_capacity"
datasheets_folder_path = "datasheets"
setup_path = datasheets_folder_path + "/setup_heat_capacity.json"

# creating paths to all needed folders and files for the newton experiment
data_folder_path = "data/data_newton"
datasheets_folder_path = "datasheets"
setup_path = datasheets_folder_path + "/setup_newton.json"

# creating metadata dict from the setup json file
metadata = m_json.get_metadata_from_setup(setup_path)

# adding serial ids to the sensor metadata
m_json.add_temperature_sensor_serials(datasheets_folder_path, metadata)

# measuring temperature and saving the data in a dict with temperature values and timestamps in two lists
data = m_pck.get_meas_data_calorimetry(metadata)

# creating data folder, saving the sensor data in h5 files and the associated metadata in a json file
m_pck.logging_calorimetry(data, metadata, data_folder_path, datasheets_folder_path)

# creating a json file with the metadata from the curent experiment in the data folder
m_json.archiv_json(datasheets_folder_path, setup_path, data_folder_path)
